package pal.co.kr.serverconnection;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private String html = "";
    private Handler mHandler;

    private Socket socket;

    private BufferedReader networkReader;
    private BufferedWriter networkWriter;
    private TextView textView;

    private String ip = "52.78.104.204"; // IP
    private int port = 9000; // PORT번호

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.text);
//
    }

    public void btnClick(View view) {

        switch (view.getId()) {
            case R.id.btnSend:
                String url = "http://ec2-52-78-104-204.ap-northeast-2.compute.amazonaws.com:9000/authUser";
                StringRequest request = new StringRequest(Request.Method.POST,url,
                        new Response.Listener<String>(){
                           public void onResponse(String response) {
                               try{
                                   System.out.println("onResponse() 호출 "+response);
                               }catch (Exception e){
                                   e.printStackTrace();
                               }
                           }
                        },

                        new Response.ErrorListener(){
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                            }
                        }
                ){
                    protected Map<String, String> getParams(){
                        Map<String, String> params = new HashMap<>();
                        params.put("id","hi");
                        params.put("password","hihihihi");

                        return params;
                    }
                };

                Volley.newRequestQueue(this).add(request);
                System.out.println("웹 서버에 요청함:"+url);
                break;
        }
    }


}
