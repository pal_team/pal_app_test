package kr.co.pal;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by JiHoon on 2016. 12. 21..
 */

@SuppressWarnings("deprecation")
public class CameraObj extends SurfaceView implements SurfaceHolder.Callback {
    private final String LOG = "CameraObj";
    private Context context;
    private SurfaceHolder holder;
    private Camera cam;

    public CameraObj(Context context) {
        super(context);
        this.context = context;
        holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        cam = Camera.open();
        cam.setDisplayOrientation(90);
        try{
            cam.setPreviewDisplay(holder);
        }catch (IOException e){
            cam.release();
            cam = null;
            Log.d(LOG, "Failed to set camera preview");
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        cam.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        cam.stopPreview();
        cam.release();
        cam = null;
    }
}
