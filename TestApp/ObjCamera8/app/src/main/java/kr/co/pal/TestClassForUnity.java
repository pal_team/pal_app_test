package kr.co.pal;

/**
 * Created by JiHoon on 2016. 12. 22..
 */

public class TestClassForUnity{
    public static TestClassForUnity getInstance = new TestClassForUnity();

    public static TestClassForUnity getGetInstance() {
        return getInstance;
    }

    private TestClassForUnity() {
    }

    public void clickObj(String msg){
        System.out.println(msg);
    }
}