package pal.co.kr.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);


    }
    public void btnClick(View view){
        Intent intent = new Intent(this, NextPage.class);
        startActivity(intent);
    }
}
