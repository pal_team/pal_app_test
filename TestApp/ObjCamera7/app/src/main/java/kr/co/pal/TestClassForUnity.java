package kr.co.pal;

import android.util.Log;

/**
 * Created by JiHoon on 2016. 12. 22..
 */

public class TestClassForUnity {
    public static TestClassForUnity MyInstance = getInstance();

    public static TestClassForUnity getInstance(){
        if(MyInstance == null){
            MyInstance = new TestClassForUnity();
        }
        return MyInstance;
    }
    private TestClassForUnity() {
    }

    @org.jetbrains.annotations.NotNull
    public static String clickObj(){
        System.out.println("Unity call this method :: JiHoon");
        return "Unity";
    }
}
