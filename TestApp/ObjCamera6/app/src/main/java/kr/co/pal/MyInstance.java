package kr.co.pal;

/**
 * Created by JiHoon on 2016. 12. 22..
 */
public class MyInstance {
    private static MyInstance ourInstance = new MyInstance();

    public static MyInstance getInstance() {
        return ourInstance;
    }

    public void clickObj(){
        System.out.println("Unity Call this Class :: JiHoon");
    }
    private MyInstance() {
    }
}
