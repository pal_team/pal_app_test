package kr.co.pal;

import android.util.Log;

/**
 * Created by JiHoon on 2016. 12. 22..
 */

public class TestClassForUnity {
    public static TestClassForUnity instance;

    public TestClassForUnity getInstance(){
        if(instance == null){
            instance = new TestClassForUnity();
        }
        return instance;
    }
    private TestClassForUnity() {
    }

    public void clickObj(String msg){
        Log.d("Unity Call function",msg);
    }
}
