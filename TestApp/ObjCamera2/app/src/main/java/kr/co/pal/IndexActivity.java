package kr.co.pal;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class IndexActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.index_activity);
    }

    public void btnClick(View view){
        Intent intent = new Intent(this, UnityPlayerActivity.class);
        switch(view.getId()){
            case R.id.one:
                intent.putExtra("param", 1);
                break;
            case R.id.ten:
                intent.putExtra("param", 10);
                break;
            case R.id.hundred:
                intent.putExtra("param", 100);
                break;
        }
        startActivity(intent);
    }
}
