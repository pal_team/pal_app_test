package kr.co.pal.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import kr.co.pal.R;
import kr.co.pal.UnityPlayerActivity;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnClick(View view){
        Intent intent = new Intent(this, UnityPlayerActivity.class);
        switch (view.getId()){
            case R.id.three:
                intent.putExtra("Obj",3);
                break;
            default:
                break;
        }
        startActivity(intent);
    }
}
