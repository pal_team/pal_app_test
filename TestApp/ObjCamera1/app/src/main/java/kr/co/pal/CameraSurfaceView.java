package kr.co.pal;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by JiHoon on 2016. 12. 21..
 */

@SuppressWarnings("deprecation")
public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback{

    private final String LOG = "CameraSurfaceView";
    private SurfaceHolder holder;
    private Camera camera;

    public CameraSurfaceView(Context context){
        super(context);
        holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        camera = Camera.open();
        camera.setDisplayOrientation(90);
        try{
            camera.setPreviewDisplay(holder);
        }catch (Exception e){
            camera.release();
            camera = null;
            Log.e(LOG,"Failed to set camera preview",e);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
//        Camera.Parameters parameters = camera.getParameters();
//        parameters.setPreviewSize(480,640);
//        camera.setParameters(parameters);
        camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camera.stopPreview();
        camera.release();
        camera = null;
    }
}